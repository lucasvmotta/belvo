from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from belvo.spiders.instacart import InstacartSpider

process = CrawlerProcess(get_project_settings())
process.crawl(InstacartSpider)
process.start()