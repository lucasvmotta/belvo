import json
import unittest
from belvo.spiders import instacart
from scrapy.selector import Selector
from belvo.items import Store, Product
from scrapy.http.request import Request

## This class is used create a fake response object that will be consumed by the spider
class FakeJsonResponse(object):
    def __init__(self, text):
        self.text = text
        self.meta = {'department_name': 'Pantry'}


class InstacartTest(unittest.TestCase):
    def setUp(self):
        self.spider = instacart.InstacartSpider
        self.spider_name = 'instacart'
        self.start_urls = ['https://instacart.com']
        self.extract_products_data = None
        self.max_department_index = 2
        self.max_product_index = 2

    def test_extract_store_name(self):
        with open(f'belvo/test/responses/{self.spider_name}/02_08_21-23:06:03_storefront.html', 'r') as f:  # noqa: E501
            fake_response = Selector(text=f.read())
            result = self.spider.extract_store_data(self, fake_response)
            store = Store()
            store["store_name"] = "LeBeau Nob Hill"
            self.assertEqual(store['store_name'], next(result)['store_name'])

    def test_extract_store_logo(self):
        with open(f'belvo/test/responses/{self.spider_name}/02_08_21-23:06:03_storefront.html', 'r') as f:  # noqa: E501
            fake_response = Selector(text=f.read())
            result = self.spider.extract_store_data(self, fake_response)
            store = Store()
            store["logo"] = "https://d2d8wwwkmhfcva.cloudfront.net/156x/d2lnr5mha7bycj.cloudfront.net/warehouse/logo/631/2293fb92-3352-4114-8025-c055d198be15.png"  # noqa: E501
            store_result = next(result)
            self.assertEqual(store['logo'], store_result['logo'])
            self.assertIsInstance(store_result, Store)

    def test_extract_department_data_extracts_three_departments(self):
        with open(f'belvo/test/responses/{self.spider_name}/02_08_21-23:06:05_department.json', 'r') as f: # noqa: E501
            json_as_string = f.read()
            fakeJsonResponse = FakeJsonResponse(text=json_as_string)
            result = self.spider.extract_department_data(self, fakeJsonResponse)
            items_urls = [
                'https://instacart.com/v3/retailers/631/module_data/department_20954?origin_source_type=store_root_department&tracking.grid_row=2&tracking.page_view_id=0d6f9c38-442d-42e4-936d-464ecb2bfc26',  # noqa: E501
                'https://instacart.com/v3/retailers/631/module_data/department_20969?origin_source_type=store_root_department&tracking.grid_row=3&tracking.page_view_id=0d6f9c38-442d-42e4-936d-464ecb2bfc26',  # noqa: E501
                'https://instacart.com/v3/retailers/631/module_data/department_20962?origin_source_type=store_root_department&tracking.grid_row=4&tracking.page_view_id=0d6f9c38-442d-42e4-936d-464ecb2bfc26'  # noqa: E501
            ]
            for index in range(0, self.max_department_index+1):
                department_result = next(result)
                self.assertIsInstance(department_result, Request)
                self.assertEqual(
                    department_result.url,
                    items_urls[index]
                )
            with self.assertRaises(StopIteration) as ve:
                next(result)

    def test_extract_products_data_extracts_three_products(self):
        with open(f'belvo/test/responses/{self.spider_name}/02_08_21-23:06:07_products.json', 'r') as f: # noqa: E501
            json_as_string = f.read()
            fakeJsonResponse = FakeJsonResponse(text=json_as_string)
            result = self.spider.extract_products_data(self, fakeJsonResponse)
            products = [
                Product(product_name='Essential Everyday Bread Flour, Enriched, Unbleached, Pre-Sifted', department='Pantry'),
                Product(product_name="Bob's Red Mill Unbleached White All-Purpose Organic Flour", department='Pantry'),
                Product(product_name="Meek's Honey Orange California Honey", department='Pantry')
            ]
            for index in range(0, self.max_department_index+1):
                product_result = next(result)
                self.assertIsInstance(product_result, Product)
                self.assertEqual(
                    product_result,
                    products[index]
                )
            with self.assertRaises(StopIteration) as ve:
                next(result)
