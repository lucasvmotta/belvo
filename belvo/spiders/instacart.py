import scrapy
import json
import re
from ..items import Product, Store
import logging





import datetime


class InstacartSpider(scrapy.Spider):
    name = 'instacart'
    allowed_domains = ['instacart.com']
    start_urls = ['https://instacart.com']
    login = 'jf.lucas@hotmail.com'
    password = 'lucasmotta'
    custom_settings = {
        'LOG_LEVEL': 'INFO',
        'LOG_FILE': f'logs/{name}.log',
    }
    max_department_index = 2
    max_product_index = 2

    # _instacart_session_id was set loggin in via browser
    # With the session token, we can access the store front page
    def start_requests(self):
        """ This function parses a sample response. Some contracts are mingled
        with this docstring.

        @url http://www.amazon.com/s?field-keywords=selfish+gene
        @returns items 1 16
        @returns requests 0 1
        @scrapes Title Author Year Price
        """
        try:
            logging.info('Accessing storefront')
            cookies = {
                '_instacart_session_id': 'K3JHcFQ3cEJ4WGRmbWY3dFl4MW55NjdpWkRlUWdnSElzQjJZY2Q1R0ZiVERzWGhsSUtuWDIvbEJ2cDlPTmQwSTltM3hqZTd5UDlTaDMwVWxXeXJYckxBS1NpNWVMVlZvUndrUXc1aGN2R1lrSFVYVmIxSUphbS9SUXdDZWtVN2QyY3RGM2UxQUc1OTQxb3N0RmFuTmVQdzJuMEpKUVpldXVaMDhxTjFWcnJ3PS0tQVozd0QzbTdZRXRRcXlCaUZsOUNkQT09--c7f7e9d7ef8d8bb93cbef0f38a5fc42f99a3e016',  # noqa: E501
            }
            store_url = f'{self.start_urls[0]}/store/lebeau-nob-hill/storefront'
            yield scrapy.Request(url=store_url, callback=self.extract_store_data, cookies=cookies)
        except Exception as e:
            logging.warning(f'Error on start_requests: {e}')

    # Inside the store page, we can extract the store's logo and name
    # To create the url to access the departments data, we need a "cache_key", which can be found
    #   inside a script with the id=node-initial-bundle. I used a simple regex to extract this data
    # We yield a store object, that will be outputed into a json file specified in the settings
    # With the cache_key in hands, we can access the json that returns the department details

    def extract_store_data(self, response):
        try:
            logging.info("Extracting data from store")
            store = Store()
            store['store_name'] = response.xpath("//h1/text()").get()
            store['logo'] = response.xpath(
                "//div[contains(@class,'header-hero-logo')]/img/@src").get()
            yield store
            node_initial_bundle_script = response.xpath(
                '//script[@id="node-initial-bundle"]').extract_first()
            cache_key = re.search(
                r'cache_key%22%3A%22(.*?)%22%2C', node_initial_bundle_script).group(1)
            departments_json_url = f'{self.start_urls[0]}/v3/containers/lebeau-nob-hill/storefront?source=web&cache_key={cache_key}'  # noqa: E501
            yield scrapy.Request(url=departments_json_url, callback=self.extract_department_data)
        except Exception as e:
            logging.warning(f'Error on extract_store_data: {e}')

    # Inside the department details json, we can get the async_data_path, which cointains a url
    #   that outputs all the products from the desired department
    # If the index of the enumerate is higher than the max departments to extract, we break the loop
    def extract_department_data(self, response):
        try:
            logging.info("Extracting data from department")
            department_data_json = json.loads(response.text)
            modules_json = department_data_json['container']['modules']
            departments_data = [module for module in modules_json if module['id'].startswith('department')]  # noqa: E501
            for index, department in enumerate(departments_data):
                products_list_json_url = department['async_data_path']
                department_name = department['data']['header']['label']
                yield scrapy.Request(
                    url=f'{self.start_urls[0]}{products_list_json_url}', 
                    callback=self.extract_products_data, 
                    meta={'department_name': department_name,}
                )
                if index == self.max_department_index:
                    break
        except Exception as e:
            logging.warning(f'Error on extract_department_data: {e}')

    # Inside the products json, we can extract each product data
    # If the index of the enumerate is higher than the max products to extract, we break the loop
    # We yield a product object, that will be outputed into a json file specified in the settings
    def extract_products_data(self, response):
        try:
            logging.info("Extracting products")
            products_data_json = json.loads(response.text)
            items_json = products_data_json['module_data']['items']
            for index, product_json in enumerate(items_json):
                product = Product()
                product['product_name'] = product_json['name']
                product['department'] = response.meta['department_name']
                yield product
                if index == self.max_product_index:

                    break
        except Exception as e:
            logging.warning(f'Error on extract_products_data: {e}')
