# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

# We enable a simple pipeline so our items get outputted into the file specified in the settings
class BelvoPipeline:
    def process_item(self, item, spider):
        return item