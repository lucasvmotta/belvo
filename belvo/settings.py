# Scrapy settings for belvo project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html


BOT_NAME = 'belvo'
SPIDER_MODULES = ['belvo.spiders']
NEWSPIDER_MODULE = 'belvo.spiders'

# We set a user_agent to not get blocked
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) Safari/537.36'
# We set a single crawler to run
CONCURRENT_REQUESTS_PER_DOMAIN = 1
# We tell the robot to ignore the robots.txt
ROBOTSTXT_OBEY = False

# We configure the outputs
FEEDS = {
   'data.json': {
      'format': 'json',
   }
}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
# We configure the pipeline
ITEM_PIPELINES = {
   'belvo.pipelines.BelvoPipeline': 300,
}