# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

# We declare a Product item
# We set the prefix product on the name field so our items (product and store) dont have the same
#   key
class Product(scrapy.Item):
    department = scrapy.Field()
    product_name = scrapy.Field()

# We declare a Product item
# We set the prefix store on the name field so our items (product and store) dont have the same
#   key
class Store(scrapy.Item):
    logo = scrapy.Field()
    store_name = scrapy.Field()
