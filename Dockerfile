# Python 3 image's selection
FROM python:3

# Setting working directory
WORKDIR /usr/src/app

# Copying requirements from localhost to container
COPY requirements.txt ./

# Installing scrapy
RUN pip3 install --no-cache-dir -r requirements.txt

# Copying source code from localhost to container
COPY . .


