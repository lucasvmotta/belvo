Mon 2/8/20201
13:00
- created project
- installed scrapy
- created spider

13:30 
- found that the page returns a string "very sorry", which I bypassed ignoring robots.txt and settings an user_agent
- clicking on 'log in' appears to call some javascript and change the frontend, showing the email and password fields
- log in button (in the modal requesting email and password) has no property showing where the post is occurring, so I started inspecting the requests and found the post url

14:00
- I could try bypassing the captcha using deathbycaptcha or a similar service, which would involve spending money, so I decided to search through the requests and cookies to see if I can bypass the login page

15:30
- looked through the cookies and identified the ones that might be linked to user authentication, after some trial and error, found that passing _instacart_session_id give access to the website

16:00
- had problems trying to extract departments through xpath, found that it can be done with the request below, still needs to find where cache_key comes from
https://www.instacart.com/v3/containers/lebeau-nob-hill/storefront?source=web&cache_key=187b04-27317-t-844

16:15
- extracted cache_key from a <script> tag using regex

16:40
- found the url to extract the json of the products from each department, starting extraction

17:00
- sucessfully extracted the data from the departments
- pause

17:30 
- created items and pipelines on the scrapy project

18:00
- configure FEEDS on the settings, to output the data to a json 

19:00
- dockerized the crawler

19:30
- created makefile to simplify the proccess of running the crawler

20:00
- implemented logs

20:30
- extracted some htmls and jsons to implement local tests with dummy data

21:00
- implemented local tests to guarantee that the methods are parsing the data correctly (the code will still break if the page changes, since the tests run with local files)

Tue 2/9/2021
10:00
- implemented git repository
- changed docker-compose commands to docker commands