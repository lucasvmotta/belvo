# Instacart's crawler

This project builds a web crawler that logs in instacart.com and extract the first 3 departments and it's first 3 products from a specific store

# Getting Started

## Install and Run

> This project was built and tested in MacOS

1. [Install Docker for Mac](https://docs.docker.com/docker-for-mac/install/)
2. Clone this project to your local environment.
3. Run `make build` and `make run` from the top level directory to start the crawler.
4. The extracted data will be outputed to the `data.json` file, inside the root of the project.
5. Additional commands can be found inside the `Makefile`, in the top level directory, or can be listed via the `make help` command.
6. `make test` will test the crawler with offline data