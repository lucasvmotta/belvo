all: help

.PHONY: help
help: $## This help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) | sort

build: ## Builds container
	docker build -t crawler .
	
run: ## Runs instacart's crawler (needsdocker images | grep crawler to exec these commands first: make up && make build) and outputs to data.json
	docker run crawler python3 ./start-instacart.py

test: ## Run test inside the container
	docker run crawler python3 -m unittest
